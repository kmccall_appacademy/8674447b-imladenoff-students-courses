class Student
  attr_reader :first_name, :last_name, :courses
  def initialize first_name, last_name
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll new_course
    courses.each { |course| raise "Not gonna happen, buddy." if new_course.conflicts_with? course }
    return if courses.include? new_course
    self.courses << new_course
    new_course.students << self
  end

  def course_load
    cload = Hash.new 0
    @courses.each do |course|
      cload[course.department] += course.credits
    end
    cload
  end
end
